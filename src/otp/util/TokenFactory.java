/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package otp.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Lucas
 */
public class TokenFactory {

    private Date timestamp;
    private ArrayList<String> tks;
    private String seed;
    private int limit = 10;
    private final long deadline = 60000;
    private long startTime;
    private boolean started;

    public TokenFactory() {
        tks = new ArrayList();
    }

    public void initializeSeed(String s) {

        byte[] hash = getHash(s);
//        System.out.println("HashSeed: " + hash);
        seed = getHex(hash);
//        System.out.println("Seed: " + seed);        
    }

    public boolean checkTime(long start) {
        long r = System.currentTimeMillis() - start;
        if (r <= 60000) {
            return true;
        }
        return false;
    }

    public String getToken() {
        if (started) {
            if (!checkTime(startTime)) {
                limit = 10;
                initToken();
            }
        } else {
            initToken();
            startTime = System.currentTimeMillis();
            started = true;
        }
        limit--;
        if (limit >= 0) {
            timestamp = new Date();
            String tk = tks.get(limit) + getCurrentDate();
//            System.out.println("gTk: " + tk);
            tks.remove(limit);

            return nextToken(tk);
        }
        return null;
    }
    
    public String getToken(String currentDate) {
        if (started) {
            if (!checkTime(startTime)) {
                limit = 10;
                initToken();
            }
        } else {
            initToken();
            startTime = System.currentTimeMillis();
            started = true;
        }
        limit--;
        if (limit >= 0) {
            timestamp = new Date();
            String tk = tks.get(limit) + currentDate;
//            System.out.println("Token Server: " + tk);
            tks.remove(limit);

            return nextToken(tk);
        }
        return null;
    }

    public void initToken() {
        String tk = nextToken(seed);
        tks.add(tk);
        for (int i = 1; i < limit; i++) {
            tk = nextToken(tk);
            tks.add(tk);
//            System.out.println(tk);
        }
    }

    private byte[] getHash(String s) {
        try {
            MessageDigest algorithm = null;
            try {
                algorithm = MessageDigest.getInstance("SHA-256");
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(TokenFactory.class.getName()).log(Level.SEVERE, null, ex);
            }
            return algorithm.digest(s.getBytes("UTF-8"));

        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(TokenFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private String getHex(byte[] hash) {
        StringBuilder hexString = new StringBuilder();
        for (byte b : hash) {
            hexString.append(String.format("%02X", 0xFF & b));
        }
        return hexString.toString();
    }

    public String getCurrentDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmm");

//        System.out.println(dateFormat.format(date)); //2014/08/06 15:59:48
        return dateFormat.format(timestamp);
    }

    public String nextToken(String tk) {
//        System.out.println(tk);
        byte[] hash = getHash(tk);
//        System.out.println("HashTk': " + hash1);
//        System.out.println("HashTk'': " + hash2);
        String token = getHex(getHash(new String(hash)));
//        System.out.println("Token: " + token);

        return token;
    }

    public boolean authenticate(String tk, String dt) {
        initToken();
        String servTk;
        System.err.println("[TK] Token Client: " + tk);
        for (int i = 0; i < 10; i++) {
            servTk = getToken(dt);
            System.err.println("[TK] My Token: "+servTk);
            if (tk.equalsIgnoreCase(servTk)) {
                return true;
            }
        }
        return false;
    }

}
