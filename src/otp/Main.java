/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package otp;

import java.util.Scanner;
import otp.client.Client;
import otp.server.Server;
import otp.util.TokenFactory;

/**
 *
 * @author Lucas
 */
public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean isServer = false;
        boolean isClient = false;
        Server server = null;
        Client client = null;

//        TokenFactory factory = new TokenFactory();
//        System.out.print("Seed: ");
//        String seed = scanner.nextLine();
//        String seedHash = factory.initializeSeed(seed);
//        String nextToken = factory.nextToken(seedHash);
        System.out.println("--- One Time Password(Laport) ---"
                + "\n\nCommands:"
                + "\n-Initialize a server: run server port "
                + "\n-Initialize a client: run client serverIp serverPort\n");
        while (true) {

            System.out.print("$otp>>");

            String command = scanner.nextLine();
            String[] cmd = command.split(" ");

            if (cmd.length > 0) {
                if (cmd[0].equalsIgnoreCase("run")) {
                    if (cmd[1].equalsIgnoreCase("server")) {
                        int port = Integer.parseInt(cmd[2]);
                        System.out.print("Enter with a new password....\n$otp-server-password>>");
                        server = new Server(port, scanner.nextLine());
                        server.start();
                        break;
                    } else if (cmd[1].equalsIgnoreCase("client")) {
                        String ip = cmd[2];
                        int port = Integer.parseInt(cmd[3]);
                        System.out.print("Enter with your password...\n$otp-client-password>>");
                        client = new Client(ip, port, scanner.nextLine());
                        client.start();
                        break;
                    }
                } else {
                    System.err.println("Command error");
                }
            }

        }

    }

}
