/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package otp.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import otp.util.TokenFactory;

/**
 *
 * @author Lucas
 */
public class Server extends Thread {

    private int PORT;
  
    private Socket clientSocket;
    private ServerSocket serverSocket;
    private PrintWriter out;
    private BufferedReader in;
    private TokenFactory tokenFactory;
    

    public Server(int port, String seed) {
        this.PORT = port;
        this.tokenFactory = new TokenFactory();
        tokenFactory.initializeSeed(seed);
    }

    @Override
    public void run() {
        try {
            try {
                serverSocket = new ServerSocket(PORT);
            } catch (IOException e) {
                System.err.println("[Server] Could not listen on port: " + PORT);
                System.exit(1);
            }
            System.err.println("[Server] Initializing.....");
            System.err.println("[Server] Waiting for connection.....");
            try {
                clientSocket = serverSocket.accept();
            } catch (IOException e) {
                System.err.println("[Server] Accept failed.");
                System.exit(1);
            }
            System.err.println("[Server] Connection successful.....");
            System.err.println("[Server] Waiting for authentication.....");
            out = new PrintWriter(clientSocket.getOutputStream(),
                    true);
            in = new BufferedReader(
                    new InputStreamReader(clientSocket.getInputStream()));
                
            String inputLine, token;
            while ((inputLine = in.readLine()) != null) {
                System.err.println("[Server] Input >> " + inputLine);
                String[] str = inputLine.split(":");
//                System.err.println("[Server] My Token: " + token);
                if (tokenFactory.authenticate(str[0],str[1])) {
                    out.println("Autentication was succesful");
                } else {
                    out.println("Autentication failed");
                }
                if (inputLine.equals("exit")) {
                    break;
                }
            }
            System.err.println("[Server]] Closing connections......");
            out.close();
            in.close();
            clientSocket.close();
            serverSocket.close();
            System.exit(0);
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            out.close();
        }
    }

}
