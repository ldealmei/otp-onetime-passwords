#OTP - One-Time Passwords

Este trabalho tem como objetivo que todos os alunos conheçam e implementem uma técnica de autenticação que use senhas descartáveis (OTP - One-Time Passwords).
Uma senha descartável só pode ser usada uma única vez.Perde sua validade após esse uso.

Atenção - O sitema deverá ser implementado usando arquitetura cliente-servidor, onde:

* Deve ter dois programas que podem rodar em maquinas diferentes.
* Os dois programas (O servidor e o Cliente conhecem a senha raiz, mas esta nunca deve ser enviada pela rede)
* O cliente deve enviar uma mensagem pedindo autenticação para o servidor.
* Usar senhas descartáveis.
* O aluno deve propor uma variante do algoritmo OTP de Lamport, visto em aula.
* Este algoritmo deve levar em consideração o tempo (segundo)
* As senhas devem ter validade de no máximo um minuto e só podem ser usadas uma única vez.
* Se o cliente digitar uma senha válida o servidor aceita, caso contrario retorna mensagem de erro.