/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package otp.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import otp.util.TokenFactory;

/**
 *
 * @author Lucas
 */
public class Client extends Thread {

    private String serverHostname;
    private int PORT;
    private String seed;
    private Socket client;
    private PrintWriter out;
    private BufferedReader in;
    private TokenFactory tokenFactory;

    public Client(String IP, int PORT, String seed) {
        this.serverHostname = IP;
        this.PORT = PORT;
        this.tokenFactory = new TokenFactory();
        tokenFactory.initializeSeed(seed);
    }    

    public void run() {
        try {
            System.out.println("[Client] Attemping to connect to host "
                    + serverHostname + " on port " + PORT);

            Socket echoSocket = null;

            try {
                // echoSocket = new Socket("taranis", 7);
                echoSocket = new Socket(serverHostname, PORT);
                out = new PrintWriter(echoSocket.getOutputStream(), true);
                in = new BufferedReader(new InputStreamReader(
                        echoSocket.getInputStream()));
                
            } catch (UnknownHostException e) {
                System.err.println("[Client] Don't know about host: " + serverHostname);
                System.exit(1);
            } catch (IOException e) {
                System.err.println("[Client] Couldn't get I/O for "
                        + "the connection to: " + serverHostname);
                System.exit(1);
            }
            
            tokenFactory.initToken();            
            Scanner scanner = new Scanner(System.in);
            String userInput,token;
            System.out.print("[Client] Enter...\n$otp-client>>");
            while ((userInput = scanner.nextLine()) != null) {
                if (!userInput.equalsIgnoreCase("exit")) {                    
                    token = tokenFactory.getToken();
                    out.println(token+":"+tokenFactory.getCurrentDate());
                    System.out.println("[Client] " + in.readLine());
                    System.out.print("[Client] Enter....\n$otp-client>>");
                } else {
                    break;
                }
            }
            System.out.println("[Client] Closing connections.....");
            out.close();
            in.close();
            echoSocket.close();
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
